.model small ;specific number of code and data segments required for this program checkout DS AND CS
.stack 100h ;allocating space for the stack segment

; This is the data segment
.data
; write all variables required
; everything we work with is in the terms bytes

message db "Hello World, This is my first program !", "$"
;db - defined byte 
;message - variable name
;$ - terminator
;when the assembler runs the program and find the dollar sign it will stop


; This is the code segment
.code

; the main process starts here
main proc

;these 2 lines are compulsory
mov ax, @data
;source - @data => Moving all the variables in the data segment into the accumulator register
;destination - ax
;all we have in data is moved into the ax register

mov ds, ax
;Addressing Modes
;source - ax
;destination - ds
;the data in the ax register is moved into ds register
;anyhting you what printed should be in the dx register 

    ; two lines above are compulsary
    mov dx, offset message
    ; offset is necessary because message is not just one byte
    ; segment offsetting
    mov ah, 09h
    ; 09 is the instruction to print to screen
    int 21h
    ; interrupts the kernel and tells it to execute the current function
    mov ax, 4C00h
    ; This is to terminate the program
    int 21h
    
main endp ; ends the main procedrue
end main ; ends the program